#include <TroykaGPS.h>
#include <SoftwareSerial.h>
#include <GPRS_Shield_Arduino.h>

// Переменная, определяющая период отправки сообщений
int time_delay = 60000;

SoftwareSerial mySerial (4, 5);
#define GPS_SERIAL mySerial

SoftwareSerial GPRS_SERIAL(10, 11);
GPS gps(GPS_SERIAL);
GPRS gprs(GPRS_SERIAL);
#define MAX_SIZE_MASS 16
// Инициализация массивов для хранения данных с GPS
char strTime[MAX_SIZE_MASS];
char strDate[MAX_SIZE_MASS];
char latitudeBase60[MAX_SIZE_MASS];
char longitudeBase60[MAX_SIZE_MASS];
// Инициализация массива для отправки данных на телефон
char sms[70];

void setup()
{
  Serial.begin(9600);
  delay(4000);
  // Ждем инициализации серийника
  while (!Serial) {}
  Serial.print("Serial init OK\r\n");
  //Установка настроек GPS модуля
  GPS_SERIAL.begin(115200);
  Serial.println("GPS init is OK on speed 115200");
  GPS_SERIAL.write("$PMTK251,9600*17\r\n");
  GPS_SERIAL.write("$RMC");
  GPS_SERIAL.end();
  GPS_SERIAL.begin(9600);
  GPRS_SERIAL.begin(9600);
  Serial.print("GPS init is OK on speed 9600");
}

void loop()
{
  int k;
    // Проверка GPS-модуля на работоспособность
    if (gps.available()) {
      // Инициализация строковых переменных
    String Latitude = "", Longitude = "", Strtime = "";
    String results1, results2, results3, results4;
    results1 = results2 = results3 = results4 = "";
    Serial.println("\nGPS available");
    // Парсим данные с GPS
    gps.readParsing();
    // Проверяем статус GPS
    switch(gps.getState()) {
      // Если все ОК
      case GPS_OK:
      Serial.println("\nGPS OK");
      // Получение данных со спутника
        gps.getLatitudeBase60(latitudeBase60, MAX_SIZE_MASS);
        gps.getLongitudeBase60(longitudeBase60, MAX_SIZE_MASS);
        gps.getTime(strTime, MAX_SIZE_MASS);
        // "Приклеивание" элементов массивов к строкам с данными
        for (int i = 0; i < MAX_SIZE_MASS; i++)
        {
          Latitude = Latitude + latitudeBase60[i];
          Longitude = Longitude + longitudeBase60[i];
          Strtime = Strtime + strTime[i];
        }
        // Запись данных в переменные результатов
        results1 = "GPS coordinates: ";
        results2 = "\nLatitude: " + Latitude;
        results3 = "\nLongitude: " + Longitude;
        results4 = "\nTime: " + Strtime;
        // Склеивание результатов замеров в один массив типа char[]
        for (int i = 0; i < 70; i++)
        {
          sms[i] = ' ';
        }
        for (int i = 0; i < results1.length(); i++)
        {
          sms[i] = results1[i];
        }
        k = results1.length();
        for (int i = 0; i < results2.length(); i++, k++)
        {
          sms[k] = results2[i];
        }
        k = results2.length();
        for (int i = 0; i < results3.length(); i++, k++)
        {
          sms[k] = results3[i];
        }
        k = results3.length();
        for (int i = 0; i < results4.length(); i++, k++)
        {
          sms[k] = results4[i];
        }
        // Включение GPRS-модуля для отправки сообщения
        gprs.powerOn();
        delay(10000);
        // Отправляем сообщение
        gprs.sendSMS("+79852770777", sms);
        Serial.write(sms);
               //while (!gprs.init()) {delay(1000); Serial.print("Init error\r\n");}
        // Выключаем GPRS-модуль для экономии электроэнергии
        gprs.powerOff();
        break;
        
        // Если пришли данные с ошибками, то отправляем сообщение "GPS error data"
      case GPS_ERROR_DATA:
        gprs.powerOn();
        delay(10000);
        gprs.sendSMS("+79852770777", "GPS error data");
        gprs.powerOff();
        Serial.println("GPS error data");
        break;
        // Если не удалось подключиться к спутникам, то отправляем сообщение об отсутствии подключения
      case GPS_ERROR_SAT:
        gprs.powerOn();
        delay(10000);
        gprs.sendSMS("+79852770777", "GPS no connect to satellites");
        gprs.powerOff();
        Serial.println("\nGPS no connect");
        break;
    }
  }
  // Если GPS-модуль не работает, то отправляем сообщение "GPS недоступен"
  else
  {
    Serial.println("\nGPS unavailable");
    gprs.powerOn();
    delay(10000);
    gprs.sendSMS("+79852770777", "GPS unavailable");
    gprs.powerOff();
  }
    // Задержка между сканированиями
    delay(time_delay);
}